package smoke

import base.BaseTest
import elements.BlockNecessaryConditionsOnMainPage
import elements.MainPage
import io.qameta.allure.Feature
import io.qameta.allure.Story
import org.testng.annotations.Test

@Feature("Смоук: Блок необходимые условия для бизнеса")
class BlockNecessaryConditionsForBusinessTest: BaseTest() {

    private val mainPage = MainPage()
    private val blockNecessaryConditionsOnMainPage = BlockNecessaryConditionsOnMainPage()
    private val headerBlock = " Необходимые условия для твоего бизнеса "

    @Test
    @Story("Проверка состава блока необходимые условия для бизнеса")
    fun verifyBlockNecessaryConditionsForBusinessTest() {
        mainPage.shouldBeLogo()
        blockNecessaryConditionsOnMainPage.shouldBeTextOnHeader(headerBlock)
        blockNecessaryConditionsOnMainPage.checkButtonRateSelect()
        blockNecessaryConditionsOnMainPage.checkPriceLabel()
    }
}