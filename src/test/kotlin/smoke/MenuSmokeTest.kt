package smoke

import base.BaseTest
import elements.MainPage
import elements.MenuOnMainPage
import io.qameta.allure.Feature
import io.qameta.allure.Step
import io.qameta.allure.Story
import org.testng.annotations.Test

@Feature("Смоук: вызов меню")
class MenuSmokeTest:BaseTest() {

    private val mainPage = MainPage()
    private val menuOnMainPage = MenuOnMainPage()
    private val headersName = arrayListOf(" Про Бланк ", " Конструктор тарифов ",
        " Специальный тариф для нового бизнеса ", " Бесплатная бухгалтерия ", " ВЭД и обмен валюты ",
        " Куайринг ", " Интернет-эквайринг ", " Торговый эквайринг ", " Проверка контрагентов ",
        " Банкоматы ", " Открытый API Бланка ", " Партнёрская программа ", " Открыть ИП или ООО ")

    @Test
    @Story("Проверка наличия элементов в открытом меню")
    fun verifyMenuElementsTest() {
        mainPage.shouldBeLogo()
        mainPage.clickMenuButton()
        menuOnMainPage.checkCompositionCalledMenu(headersName)
    }
}