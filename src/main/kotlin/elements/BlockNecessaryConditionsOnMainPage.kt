package elements

import com.codeborne.selenide.Condition.text
import com.codeborne.selenide.Condition.visible
import com.codeborne.selenide.Selenide
import io.qameta.allure.Step

class BlockNecessaryConditionsOnMainPage {

    private val headerBlock = Selenide.`$`("h2.subtitle.max-w-none")
    private val buttonRateSelect = Selenide.`$`("button.btn.btn--primary.btn--hover-marketing")
    private val priceLabel = Selenide.`$`("span.price-label.d-flex.flex-as-stretch")

    @Step("Проверяем текст заголовка {headerName}")
    fun shouldBeTextOnHeader(headerName: String) {
        headerBlock.shouldBe(visible).shouldHave(text(headerName))
    }

    @Step("Проверяем наличие кнопки выбрать этот тариф")
    fun checkButtonRateSelect() {
        buttonRateSelect.shouldBe(visible)
    }

    @Step("Проверяем наличие ценника")
    fun checkPriceLabel() {
        priceLabel.shouldBe(visible)
    }

}