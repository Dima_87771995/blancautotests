package elements

import com.codeborne.selenide.Condition.text
import com.codeborne.selenide.Condition.visible
import com.codeborne.selenide.ElementsCollection
import com.codeborne.selenide.Selenide
import io.qameta.allure.Step

class MenuOnMainPage {

    private val closeButton = Selenide.`$`("span.btn-component__inner")
    private val menuElements: ElementsCollection = Selenide.`$$`("a.menu-link.mb-12")

    @Step("Проверяем состав вызываемого меню")
    fun checkCompositionCalledMenu(headerNames: ArrayList<String>) {
        closeButton.shouldBe(visible)
        headerNames.forEachIndexed { index, value -> menuElements[index].shouldHave(text(value)) }

    }
}