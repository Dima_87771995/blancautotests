package elements

import com.codeborne.selenide.Condition.enabled
import com.codeborne.selenide.Condition.visible
import com.codeborne.selenide.Selenide
import io.qameta.allure.Step

class MainPage {

    val logoBlanc = Selenide.`$`("svg.logo.svg")
    val menuButton = Selenide.`$`("button.burger-btn")
    val persAreaButton = Selenide.`$`("a.btn.btn--secondary")

    @Step("Проверяем наличие логотипа Бланк")
    fun shouldBeLogo() {
        logoBlanc.shouldBe(visible)
    }

    @Step("Проверяем наличие кнопки персональный кабинет")
    fun shouldBePersAreaButton() {
        persAreaButton.shouldBe(visible)
    }

    @Step("Кликаем на кнопку меню")
    fun clickMenuButton() {
        menuButton.shouldBe(enabled).click()
    }
}