package base

import com.codeborne.selenide.Configuration
import com.codeborne.selenide.Selenide
import org.testng.annotations.AfterClass
import org.testng.annotations.BeforeClass

open class BaseTest {

    @BeforeClass(alwaysRun = true)
    fun setUp() {
        Configuration.browser = "chrome"
        Configuration.browserSize = "2100x1080"
        Configuration.timeout = 5000

        Selenide.open("https://blanc.ru/")
    }

    @AfterClass
    fun tearDown() {
        Selenide.closeWebDriver()
    }
}